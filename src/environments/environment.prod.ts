import { ApiUrls } from '../app/Constants/apis';

export const environment = {
  production: true,
  apis: ApiUrls.prod
};
