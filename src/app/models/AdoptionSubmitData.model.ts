export class AdoptionSubmitData {
    RepID: string;
    RepName: string;
    RepEmail?: string;
    hcpName: string;
    hcpID: string;
    hcpEmail?: string;
    specialty: string;
    adoption: string;
    hcpPerRep?: number;
}
