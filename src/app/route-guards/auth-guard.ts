import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalEntries } from '../Constants/localEntries';

@Injectable()
export class AuthGuard implements CanActivate {
    public constructor(private router: Router) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // check on local storage for rep DB_ID
        if (localStorage.getItem(LocalEntries.repData) !== null) { return true; }

        // Store the attempted URL for redirecting
        console.log('return url', state.url);
        const url = state.url === '/repLogin?' ? '/' : state.url;
        localStorage.setItem(LocalEntries.attemptedURL, url);

        // Navigate to the login page
        this.router.navigate(['/repLogin'], {queryParams: route.queryParams, fragment: route.fragment});


        return false;
    }
}
