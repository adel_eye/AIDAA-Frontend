const prodUrl = 'http://aida.eye-ltd.com/';
const testUrl = 'http://localhost:3000/';

export const ApiUrls = {
  prod: {
    repLogin: prodUrl + 'api/repLogin',
    attemptsNo: prodUrl + 'api/rep_doc_attempts',
    adoption: prodUrl + 'api/adoption'
  },
  test: {
    repLogin: testUrl + 'api/repLogin',
    attemptsNo: testUrl + 'api/rep_doc_attempts',
    adoption: testUrl + 'api/adoption'
  }
};

