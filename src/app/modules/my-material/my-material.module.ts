import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatInputModule, MatTableModule, MatFormFieldModule } from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    // MatInputModule,
    MatTableModule,
    MatFormFieldModule
  ],
  declarations: [],
  exports: [
    MatButtonModule,
    // MatInputModule,
    MatTableModule,
    MatFormFieldModule
  ]
})
export class MyMaterialModule { }
