import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../../services/login/login.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalEntries } from '../../Constants/localEntries';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  subscription: Subscription;
  repID = new FormControl('', [Validators.required]);
  constructor(private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    console.log(this.activatedRoute.snapshot.queryParams);
    console.log(this.activatedRoute.snapshot.fragment);
    if (localStorage.getItem(LocalEntries.repData) !== null) {
      this.router.navigate(['/adoption'], {
        queryParams: this.activatedRoute.snapshot.queryParams,
        fragment: this.activatedRoute.snapshot.fragment
      });
    }
  }

  ngOnInit() {
  }

  checkLogin() {
    console.log('login start');
    console.log(this.repID.value);
    this.subscription = this.loginService.checkRepLogin(this.repID.value).subscribe((res) => {
      console.log(res);
      if (res.success) {
        localStorage.setItem(LocalEntries.repData, JSON.stringify(res.docs));
        this.router.navigate(['/adoption'], {
          queryParams: this.activatedRoute.snapshot.queryParams,
          fragment: this.activatedRoute.snapshot.fragment
        });
      }
    });
  }
}
