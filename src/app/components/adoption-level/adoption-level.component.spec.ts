import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionLevelComponent } from './adoption-level.component';

describe('AdoptionLevelComponent', () => {
  let component: AdoptionLevelComponent;
  let fixture: ComponentFixture<AdoptionLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
