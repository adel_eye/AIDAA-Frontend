import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { HashParamsService } from '../../services/hashParams/hash-params.service';
import { AdoptionService } from '../../services/adoptionLevel/adoption.service.service';
import { LocalEntries } from '../../Constants/localEntries';
import { AdoptionSubmitData } from '../../models/AdoptionSubmitData.model';
import { AttemptSubmitData } from '../../models/AttemptsSubmitData';

@Component({
  selector: 'app-adoption-level',
  templateUrl: './adoption-level.component.html',
  styleUrls: ['./adoption-level.component.css']
})
export class AdoptionLevelComponent implements OnInit {

  option = 1;
  position = 'pos1';
  showForm = true;
  showSuccess = false;
  showError = false;
  errorMsg = '';

  statusModel = [
    '',
    'Attention',
    'Interest',
    'Desire',
    'Action',
    'Advocate'
  ];

  adoptionData = new AdoptionSubmitData();

  attemptData = new AttemptSubmitData();
  lastAttempt = {
    value: '',
    date: ''
  };

  constructor(private hashParams: HashParamsService, private adoptionService: AdoptionService) {
    // console.log(this.hashParams.hashParams('hcpName'));
    // setting up prm data in the object
    const repData = JSON.parse(localStorage.getItem(LocalEntries.repData));

    this.adoptionData.RepID = repData.RepID;
    this.adoptionData.RepName = this.hashParams.hashParams('rep');
    this.adoptionData.hcpName = this.hashParams.hashParams('hcpName');
    this.adoptionData.hcpEmail = this.hashParams.hashParams('hcpEmail');
    this.adoptionData.hcpID = this.hashParams.hashParams('exID');
    this.adoptionData.specialty = this.hashParams.hashParams('specialty');

    // getting back the last attempts
    this.attemptData = {
      hcp_id: this.adoptionData.hcpID,
      rep_id: this.adoptionData.RepID
    };

    this.adoptionService.getAttemptsNumber(this.attemptData).subscribe(res => {
      if (res.success) {
        this.lastAttempt.value = res.docs[0].status.name;
        this.lastAttempt.date = res.docs[0].history[0].created_at;
      }
    }, error => { });
  }

  ngOnInit() {
  }

  getPosition() {
    return this.position;
  }

  ngOnChange(): void {
    console.log('on-change');
  }

  onChange(): void {
    console.log('changed');
  }

  startService() {
    console.log('start services ', this.option);

    this.adoptionData.adoption = this.statusModel[this.option];
    console.log(this.adoptionData);
    this.adoptionService.submitAdoptionLevel(this.adoptionData).subscribe((res) => {
      console.log(res);
      if (res.success) {
        this.showSuccess = true;
        this.showForm = false;
      } else {
        this.showError = true;
        this.showForm = false;
      }
    }, error => {
      this.showError = true;
      this.showForm = false;
      this.errorMsg = error.message;
    });
  }

  retry() {
    console.log('clicked retry ');
    this.showError = false;
    this.showForm = true;
  }

  valueChange(value) {
    console.log('click to send with selected : ', value);
    switch (value) {
      case '1':
        this.option = 1;
        this.position = 'pos1';
        break;
      case '2':
        this.option = 2;
        this.position = 'pos2';
        break;
      case '3':
        this.option = 3;
        this.position = 'pos3';
        break;
      case '4':
        this.option = 4;
        this.position = 'pos4';
        break;
      case '5':
        this.option = 5;
        this.position = 'pos5';
        break;
    }
  }

}
