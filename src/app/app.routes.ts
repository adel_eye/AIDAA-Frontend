/**
 * Created by adel1 on 12/17/2016.
 */
import { Routes } from '@angular/router';
import { AdoptionLevelComponent } from './components/adoption-level/adoption-level.component';
import { LoginComponent } from './components/login/login.component';
import { ReportsComponent } from './components/reports/reports.component';

export const AppRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'adoption',
        component: AdoptionLevelComponent
    },
    {
        path: '',
        component: ReportsComponent
    }
];
