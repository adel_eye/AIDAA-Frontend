import { TestBed, inject } from '@angular/core/testing';

import { HashParamsService } from './hash-params.service';

describe('HashParamsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HashParamsService]
    });
  });

  it('should be created', inject([HashParamsService], (service: HashParamsService) => {
    expect(service).toBeTruthy();
  }));
});
