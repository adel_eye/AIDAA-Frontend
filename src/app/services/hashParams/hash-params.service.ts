import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HashParamsService {

  constructor() { }

  hashParams(paramName) {
    const pageUrl = decodeURIComponent(window.location.hash.substring(1));

    if (this.getUrlParameter('prm', pageUrl)) {
      let prm = this.getUrlParameter('prm', pageUrl);


      prm = atob(prm);

      // console.log(prm);

      // console.log(decodeURIComponent(this.getUrlParameter(paramName, prm)));
      return decodeURIComponent(this.getUrlParameter(paramName, prm));
    }
  }

  private getUrlParameter(sParam, linkToSearch) {
    if (linkToSearch) {

      const sURLVariables = linkToSearch.split('&');
      let sParameterName,
        i;

      for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : sParameterName[1];
        }
      }
    } else {
      // alert("Please be sure that you get to that page from MI");
      return false;
    }
  }
}
