import { TestBed, inject } from '@angular/core/testing';

import { Adoption.ServiceService } from './adoption.service.service';

describe('Adoption.ServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Adoption.ServiceService]
    });
  });

  it('should be created', inject([Adoption.ServiceService], (service: Adoption.ServiceService) => {
    expect(service).toBeTruthy();
  }));
});
