import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdoptionService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  public submitAdoptionLevel(adoptionSubmitData: any): Observable<any> {
    return this.http.post(environment.apis.adoption, adoptionSubmitData, this.httpOptions);
  }

  public getAttemptsNumber(HcpRepIds: any): Observable<any> {
    return this.http.post(environment.apis.attemptsNo, HcpRepIds, this.httpOptions);
  }
}
